function getCurrentHour() {
    return new Date().getHours();
}

const updateURL = async () => {
    let currentDate = await obtenerHoraDesdeNTP();

    let imgElement = document.querySelector("#image-replace");

    const currentHours = getCurrentHours(currentDate);
    const currentDay = getCurrentDay(currentDate);
    console.log("H:", currentHours);
    // Lunes
    if (currentDay === 1){
        switch (true) {
            case currentHours >= "00:00:00" && currentHours <= "05:00:00":
                imgElement.src =
                "https://radiografica.org.ar/wp-content/uploads/2022/03/00-DEJALA-TODA.png";
            break;
            case currentHours > "05:00:00" && currentHours <= "07:00:00":
                imgElement.src =
                "https://radiografica.org.ar/wp-content/uploads/2022/03/00-BOCHA-DE-TANGO.png";
            break;
            case currentHours >= "14:00:00" && currentHours <= "16:00:00":
                imgElement.src =
                "https://radiografica.org.ar/wp-content/uploads/2022/03/EL-CIELO-POR-ASALTO.png";
            break;
            case currentHours > "16:00:00" && currentHours <= "18:00:00":
                imgElement.src =
                "https://radiografica.org.ar/wp-content/uploads/2022/03/ABRI-LA-CANCHA.png";
            break;
            case currentHours > "18:00:00" && currentHours <= "20:00:00":
                imgElement.src =
                "https://radiografica.org.ar/wp-content/uploads/2022/03/LA-SENAL.png";
            break;
            case currentHours > "20:00:00" && currentHours <= "21:00:00":
                imgElement.src =
                "https://radiografica.org.ar/wp-content/uploads/2022/03/RESISTIENDO.png";
            break;
            case currentHours > "20:00:00" && currentHours <= "21:00:00":
                imgElement.src =
                "https://radiografica.org.ar/wp-content/uploads/2022/03/RESISTIENDO.png";
            break;
            default:
            // Default image source if the current hour is not explicitly h&&led
            imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/00-MUSICA-EN-LA-GRAFICA.png"; // Add your default image URL
            break;
        }
    }
    // Martes
    else if (currentDay === 2){
        switch (true){
			case (currentHours > "02:00:00" && currentHours <= "05:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/00-DEJALA-TODA.png";
				break;
			case (currentHours > "05:00:00" && currentHours <= "07:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/00-BOCHA-DE-TANGO.png";
				break;
			case (currentHours > "08:00:00" && currentHours <= "10:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/PUNTO-DE-PARTIDA.png";
				break;
			case (currentHours > "10:00:00" && currentHours <= "13:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/DESDE-EL-BARRIO.png";
				break;				
			case (currentHours >= "15:00:00" && currentHours <= "16:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/INTERESES-COLECTIVOS.png";
				break;
			case (currentHours > "16:00:00" && currentHours <= "18:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/ABRI-LA-CANCHA.png";
				break;
			case (currentHours > "18:00:00" && currentHours <= "20:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/LA-SENAL-CAFE.png";
				break;
			case (currentHours > "20:00:00" && currentHours <= "21:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/RESISTIENDO.png";
				break;
			default:
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/00-MUSICA-EN-LA-GRAFICA.png";
				break;
		}
    }
    // Miercoles
    else if (currentDay === 3){
        switch (true){
			case (currentHours >= "00:00:00" && currentHours <= "05:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/00-DEJALA-TODA.png";
				break;
			case (currentHours > "05:00:00" && currentHours <= "07:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/00-BOCHA-DE-TANGO.png";
				break;
			case (currentHours > "08:00:00" && currentHours <= "10:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/PUNTO-DE-PARTIDA.png";
				break;
			case (currentHours > "10:00:00" && currentHours <= "13:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/DESDE-EL-BARRIO.png";
				break;
			case (currentHours > "15:00:00" && currentHours <= "16:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/VAMOS.png";
				break;
			case (currentHours > "16:00:00" && currentHours <= "18:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/ABRI-LA-CANCHA.png";
				break;
			case (currentHours > "18:00:00" && currentHours <= "20:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/LA-SENAL-YA-NADA.png";
				break;
			case (currentHours > "20:00:00" && currentHours <= "21:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/RESISTIENDO.png";
				break;
			case (currentHours > "21:00:00" && currentHours <= "22:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/QUEDA-LA-PALABRA.png";
				break;
			default:
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/00-MUSICA-EN-LA-GRAFICA.png";
				break;
		}
    }
    // Jueves
    else if (currentDay === 4){
        switch (true){
			case (currentHours > "00:00:00" && currentHours <= "05:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/00-DEJALA-TODA.png";
				break;
			case (currentHours > "05:00:00" && currentHours <= "07:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/00-BOCHA-DE-TANGO.png";
				break;
			case (currentHours > "08:00:00" && currentHours <= "10:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/PUNTO-DE-PARTIDA.png";
				break;
			case (currentHours > "10:00:00" && currentHours <= "13:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/DESDE-EL-BARRIO.png";
				break;
			case (currentHours > "13:00:00" && currentHours <= "14:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/07/TAXI-HOY.png";
				break;			
			case (currentHours > "14:00:00" && currentHours <= "16:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2023/02/ADN-POPULAR.png";
				break;					
			case (currentHours > "16:00:00" && currentHours <= "18:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/ABRI-LA-CANCHA.png";
				break;
			case (currentHours > "18:00:00" && currentHours <= "20:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/PATRIA-GR&&E.png";
				break;
			case (currentHours > "20:00:00" && currentHours <= "21:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/RESISTIENDO.png";
				break;
			case (currentHours > "21:00:00" && currentHours <= "22:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/SIEMPRE-LIBRE.png";
				break;
			case (currentHours > "22:00:00" && currentHours <= "23:59"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/LA-PUERTA.png";
				break;
			default:
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/00-MUSICA-EN-LA-GRAFICA.png";
				break;
		}
    }
    // Viernes
    else if (currentDay === 5){
        switch (true){
			case (currentHours > "00:00:00" && currentHours <= "05:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/00-DEJALA-TODA.png";
				break;
			case (currentHours > "05:00:00" && currentHours <= "07:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/00-BOCHA-DE-TANGO.png";
				break;
			case (currentHours > "08:00:00" && currentHours <= "10:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/PUNTO-DE-PARTIDA.png";
				break;
			case (currentHours > "10:00:00" && currentHours <= "13:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/DESDE-EL-BARRIO.png";
				break;
			case (currentHours >= "13:00:00" && currentHours <= "14:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2023/02/HISTORIETAS-DE-POLITICA.png";
				break;
			case (currentHours >= "14:00:00" && currentHours <= "15:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/LEGISLATURA.png";
				break;
			case (currentHours >= "15:00:00" && currentHours <= "16:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/HORA-LIBRE.png";
				break;
			case (currentHours > "16:00:00" && currentHours <= "18:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/ABRI-LA-CANCHA.png";
				break;
			case (currentHours > "18:00:00" && currentHours <= "19:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/ESPECIALES-RG.png";
				break;
			case (currentHours > "19:00:00" && currentHours <= "20:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/LA-SENAL.png";
				break;
			case (currentHours > "20:00:00" && currentHours <= "21:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/RESISTIENDO.png";
				break;
			case (currentHours > "22:00:00" && currentHours <= "23:59"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2023/02/TODO-GUD.png";
				break;
			default:
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/00-MUSICA-EN-LA-GRAFICA.png";
				break;
		}
    }
    // Sabado
    else if (currentDay === 6){
        switch (true){
			case (currentHours > "00:00:00" && currentHours <= "02:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/REFUGIO.png";
				break;
			case (currentHours > "02:00:00" && currentHours <= "05:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/00-DEJALA-TODA.png";
				break;
			case (currentHours > "05:00:00" && currentHours <= "07:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/00-BOCHA-DE-TANGO.png";
				break;
			case (currentHours > "07:00:00" && currentHours <= "08:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/ESPECIALES-RG-1.png";
				break;
			case (currentHours > "08:00:00" && currentHours <= "09:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/TEJIENDO.png";
				break;
			case (currentHours > "09:00:00" && currentHours <= "10:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2023/02/DE-ACA-PARA-ALLA.png";
				break;
			case (currentHours > "10:00:00" && currentHours <= "13:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/FEAS.png";
				break;
			case (currentHours >= "13:00:00" && currentHours <= "15:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/PARECEMOS.png";
				break;
			case (currentHours > "16:00:00" && currentHours <= "18:00:00" ):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/DE-FOGON.png";
				break;
			case (currentHours > "18:00:00" && currentHours <= "20:00:00" ):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2023/02/ENFOQUE-RADIO.png";
				break;
			case (currentHours > "20:00:00" && currentHours <= "22:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/FIEBRE-DE-SABADO.png";
				break;
			case (currentHours > "22:00:00" && currentHours <= "23:59"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2023/02/RIMAS-REBELDES.png";
				break;
			default:
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/00-MUSICA-EN-LA-GRAFICA.png";
				break;
		}
    }
    // Domingo
    else if (currentDay === 0){
        switch (true){
			case (currentHours > "00:00:00" && currentHours <= "02:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/04/PLAN-SONORO.png";
				break;
				case (currentHours > "02:00:00" && currentHours <= "05:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/00-DEJALA-TODA.png";
				break;
			case (currentHours > "05:00:00" && currentHours <= "07:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/00-BOCHA-DE-TANGO.png";
				break;
			case (currentHours > "08:00:00" && currentHours <= "09:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/VOCES-MAESTRAS.png";
				break;
			case (currentHours > "09:00:00" && currentHours <= "10:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/ESPECIALES-RG-1.png";
				break;
			case (currentHours > "10:00:00" && currentHours <= "11:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/ABRAMOS-LA-BOCA-1.png";
				break;
			case (currentHours > "11:00:00" && currentHours <= "12:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/04/CHARLAS-VESTUARIO.png";
				break;				
			case (currentHours > "12:00:00" && currentHours <= "13:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/CANTO-MAESTRO.png";
				break;
			case (currentHours > "13:00:00" && currentHours <= "14:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/SUMEMOS.png";
				break;
			case (currentHours > "14:00:00" && currentHours <= "15:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/ESPECIALES-RG-1.png";
				break;
			case (currentHours >= "15:00:00" && currentHours <= "16:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/FABRIC&&O.png";
				break;
			case (currentHours > "16:00:00" && currentHours <= "17:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/MALVINAS.png";
				break;
			case (currentHours > "19:00:00" && currentHours <= "20:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/ROJOS-DE-PASION.png";
				break;
			case (currentHours > "21:00:00" && currentHours <= "22:00:00"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/ABRI-LA-CANCHA.png";
				break;
			case (currentHours > "22:00:00" && currentHours <= "23:59"):
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/RESISTIENDO.png";
				break;
			default:
				imgElement.src = "https://radiografica.org.ar/wp-content/uploads/2022/03/00-MUSICA-EN-LA-GRAFICA.png";
				break;
		}
    }

    
};













const obtenerHoraDesdeNTP = async () => {
try {
const response = await fetch(
"http://worldtimeapi.org/api/timezone/America/Argentina/Cordoba"
);
const data = await response.json();
return data;
} catch (error) {
console.error("Error al obtener la hora desde NTP:", error);
return null;
}
};

const getCurrentHours = (data) => {
var fechaHoraString = data.datetime;

var fechaHora = new Date(fechaHoraString);

// Obtener la hora, los minutos y los segundos
var horas = fechaHora.getHours();
var minutos = fechaHora.getMinutes();
var segundos = fechaHora.getSeconds();

// Formatear la hora como desees
var horaFormateada =
horas +
":" +
(minutos < 10 ? "0" : "") +
minutos +
":" +
(segundos < 10 ? "0" : "") +
segundos;

return horaFormateada;
};

const getCurrentDay = (data) => {
return data.day_of_week;
};

// Llamar a la función para obtener la hora desde NTP
obtenerHoraDesdeNTP();

updateURL();
