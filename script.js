/**
 * Jegtheme
 */

const JNEWS_THEME_URL = get_parent_theme_file_uri();
const JNEWS_THEME_FILE = __FILE__;
const JNEWS_THEME_DIR = plugin_dir_path(__FILE__);
const JNEWS_THEME_NAMESPACE = 'JNews_';
const JNEWS_THEME_CLASSPATH = JNEWS_THEME_DIR + 'class/';
const JNEWS_THEME_CLASS = 'class/';
const JNEWS_THEME_ID = 20566392;

// TGM
if (is_admin()) {
    require(get_parent_theme_file_path('tgm/plugin-list.php'));
}
class Init {
    static getInstance() {
        // Lógica de la instancia en JavaScript
        console.log('Instancia de Init creada en JavaScript');
    }
}

Init.getInstance();

date_default_timezone_set("America/Argentina/Buenos_Aires");


function shortcode_radio() {
    var fecha = new Date();
    var hora = fecha.getHours() + ":" + (fecha.getMinutes() < 10 ? '0' : '') + fecha.getMinutes();

    var diasemana = fecha.getDay();

    // Lunes
    if (diasemana === 1) {
        var programa;

        switch (true) {
            case (hora > "00:00" && hora <= "05:00"):
                programa = '<div id="prueba"><a href="http://stream-radiografica.ddns.net:8110/api" target="_blank" onclick="window.open(this.href, \'width=240,height=220\'); return false;"><img src="https://radiografica.org.ar/wp-content/uploads/2022/03/00-DEJALA-TODA.png"></a></div>';
                break;
            case (hora > "05:00" && hora <= "07:00"):
                programa = '<div id="prueba"><a href="http://stream-radiografica.ddns.net:8110/api" target="_blank" onclick="window.open(this.href, \'width=240,height=220\'); return false;"><img src="https://radiografica.org.ar/wp-content/uploads/2022/03/00-BOCHA-DE-TANGO.png"></a></div>';
                break;
            case (hora > "08:00" && hora <= "10:00"):
                programa = '<div id="prueba"><a href="http://stream-radiografica.ddns.net:8110/api" target="_blank" onclick="window.open(this.href, \'width=240,height=220\'); return false;"><img src="https://radiografica.org.ar/wp-content/uploads/2022/03/PUNTO-DE-PARTIDA.png"></a></div>';
                break;
            case (hora > "10:00" && hora <= "13:00"):
                programa = '<div id="prueba"><a href="http://stream-radiografica.ddns.net:8110/api" target="_blank" onclick="window.open(this.href, \'width=240,height=220\'); return false;"><img src="https://radiografica.org.ar/wp-content/uploads/2022/03/DESDE-EL-BARRIO.png"></a></div>';
                break;
            case (hora >= "14:00" && hora <= "16:00"):
                programa = '<div id="prueba"><a href="http://stream-radiografica.ddns.net:8110/api" target="_blank" onclick="window.open(this.href, \'width=240,height=220\'); return false;"><img src="https://radiografica.org.ar/wp-content/uploads/2022/03/EL-CIELO-POR-ASALTO.png"></a></div>';
                break;
            case (hora > "16:00" && hora <= "18:00"):
                programa = '<div id="prueba"><a href="http://stream-radiografica.ddns.net:8110/api" target="_blank" onclick="window.open(this.href, \'width=240,height=220\'); return false;"><img src="https://radiografica.org.ar/wp-content/uploads/2022/03/ABRI-LA-CANCHA.png"></a></div>';
                break;
            case (hora > "18:00" && hora <= "20:00"):
                programa = '<div id="prueba"><a href="http://stream-radiografica.ddns.net:8110/api" target="_blank" onclick="window.open(this.href, \'width=240,height=220\'); return false;"><img src="https://radiografica.org.ar/wp-content/uploads/2022/03/LA-SENAL.png"></a></div>';
                break;
            case (hora > "20:00" && hora <= "21:00"):
                programa = '<div id="prueba"><a href="http://stream-radiografica.ddns.net:8110/api" target="_blank" onclick="window.open(this.href, \'width=240,height=220\'); return false;"><img src="https://radiografica.org.ar/wp-content/uploads/2022/03/RESISTIENDO.png"></a></div>';
                break;
            default:
                programa = '<div id="prueba"><a href="http://stream-radiografica.ddns.net:8110/api" target="_blank" onclick="window.open(this.href, \'width=240,height=220\'); return false;"><img src="https://radiografica.org.ar/wp-content/uploads/2022/03/00-MUSICA-EN-LA-GRAFICA.png"></a></div>';
                break;
        }

        document.write(programa);
    }
}
